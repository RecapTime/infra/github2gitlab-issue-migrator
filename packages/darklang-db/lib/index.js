const axios = require('axios').default;

/**
 * Create an new entry in your Darklang datastore
 * 
 * @param {string} baseUrl Your Darklang URL, in form of username-app-name.builtwithdark.com, where username-app-name is your canvas name.
 * @param {string} dbPassword API Key you set as an secret key in your canvas.
 * @param {Object} contents JSON object for your data, in form of key-value pairs in JSON, like what would do in Redis.
 * @returns {Object} HTTP status code, entry UUID returned from API response and HTTP response headers from axios instance.
 */
function createNewEntry(baseUrl, dbPassword, contents) {
    const token = `token ${dbPassword}`;
    const result = axios.post({
        url: "/api/db/newEntry",
        baseUrl: baseUrl,
        headers: {
            "Authorization": token
        },
        data: contents
    });
    return {
        status: result.status,
        entryUuid: result.data.uuid,
        headers: result.headers
    }
}

function queryEntry(baseUrl, dbPassword, uuid) {
    consttoken = `token ${dbPassword}`;
    const result = axios.post({
        url: "/api/db/queryById",
        baseUrl: baseUrl,
        headers: {
            "Authorization": token
        },
        data: `id=${uuid}`
    })
}

function deleteEntry(baseUrl, dbPassword, uuid) {
    const token = `token ${dbPassword}`;
    const result = axios({
        url: "/api/db/delete",
        baseUrl: baseUrl,
        headers: {
            "Authorization": token
        },
        data: `id=${uuid}`,
        method: 'delete'
    })
    return {
        status: result.status,
        isDeleted: result.data.isDeleted,
        headers: result.headers
    }
}

module.exports = {
    createNewEntry,
    queryEntry,
    deleteEntry
}