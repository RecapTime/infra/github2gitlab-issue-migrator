const darkDatastore = require("../index");
const nock = require("nock");

describe('Test entry creation', () => {
    // they're exists for API testing
    const exampleApiHost = "https://recaptime-darkalangdb-wrapper-tests.builtwithdark.com" //for demo purposes only, do not use me in prod!
    const exampleApiKey = "heyVsauceMikedmoyHere" // cursed password, do not reuse this

    test("create a new entry with value world on key hello and return an uuid", async () => {
        const response = darkDatastore.createNewEntry(exampleApiHost, exampleApiKey, {
            hello: "world"
        })
        
    });
})