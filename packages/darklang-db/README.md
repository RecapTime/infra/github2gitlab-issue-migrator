# `@recaptime/darklang-db`

An package for accessing your Darklang Datastore through regular HTTP, the REST way. Used in `gh2gl-issue-migrator` for persistence, currently in prototyping stage.

## TODO

* [ ] Support for passing axios config in the future
* [ ] Same APIs as your `mongoose` for MongoDB databases, something like `darkDatastore`.
  * [ ] There's no `darkDatastore.connect` yet, but the implementation in the backend will be something like `/api/db/ping` for checking auth creds.
* [ ] Support for more than 1 datastores, because why not?
* [ ] Make Jest tests work, currently WIP as library code itself
