FROM node:16-slim

WORKDIR /usr/src/app
COPY package.json yarn.lock .pnp.cjs .pnp.loader.mjs .yarn packages/ server/package.json ./
RUN yarn install
ENV NODE_ENV="production"
COPY . .
CMD [ "yarn", "probot:start" ]
