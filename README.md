# GitHub To GitLab Issues Migrator

> A GitHub App built with [Probot](https://github.com/probot/probot) that help you migrate GitHub issues to GitLab automgaically through webhook magic or manually through the API, with Darklang Datastore as backend.

## Setup

```sh
# We have Zero-Installs + Plug'nPlay enabled here, but just in case $#!t happens :)
yarn

# Run the bot
yarn probot:start
```

## Docker

```sh
# 1. Build container
docker build -t gh2gl-issue-migrator .

# 2. Start container
docker run -e APP_ID=<app-id> -e PRIVATE_KEY=<pem-value> gh2gl-issue-migrator
```

## Contributing

If you have suggestions for how gh2gl-issue-migrator could be improved, or want to report a bug, open an issue! We'd love all and any contributions.

For more, check out the [Contributing Guide](CONTRIBUTING.md).

## License

[AGPLv3 or later](LICENSE) © 2021 Andrei Jiroh Eugenio Halili <ajhalili2006@gmail.com>
